/*    */ package me.xCyanide.claimlevels.utils;
/*    */ 
/*    */ import java.util.Arrays;
/*    */ import org.bukkit.inventory.ItemStack;
/*    */ import org.bukkit.inventory.meta.ItemMeta;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ItemUtils
/*    */ {
/*    */   public static ItemStack signItemStack(ItemStack is, String name, String[] lore)
/*    */   {
/* 14 */     ItemMeta meta = is.getItemMeta();
/* 15 */     meta.setDisplayName(name);
/* 16 */     meta.setLore(Arrays.asList(lore));
/* 17 */     is.setItemMeta(meta);
/* 18 */     return is;
/*    */   }
/*    */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/utils/ItemUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */