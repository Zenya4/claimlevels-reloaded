/*     */ package me.xCyanide.claimlevels.utils;
/*     */ 
/*     */ import com.google.common.base.Charsets;
/*     */ import com.google.gson.Gson;
/*     */ import java.io.BufferedReader;
/*     */ import java.io.DataOutputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStreamReader;
/*     */ import java.net.HttpURLConnection;
/*     */ import java.net.Proxy;
/*     */ import java.net.URL;
/*     */ import java.net.URLConnection;
/*     */ import java.util.Scanner;
/*     */ import java.util.UUID;
/*     */ import org.json.simple.JSONObject;
/*     */ import org.json.simple.parser.JSONParser;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class UUIDLibrary
/*     */ {
/*  28 */   private static Gson gson = new Gson();
/*     */   
/*  30 */   public static String getNameFromUUID(String uuid) { String name = null;
/*     */     try {
/*  32 */       URL url = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid);
/*  33 */       URLConnection connection = url.openConnection();
/*  34 */       Scanner jsonScanner = new Scanner(connection.getInputStream(), "UTF-8");
/*  35 */       String json = jsonScanner.next();
/*  36 */       JSONParser parser = new JSONParser();
/*  37 */       Object obj = parser.parse(json);
/*  38 */       name = (String)((JSONObject)obj).get("name");
/*  39 */       jsonScanner.close();
/*     */     } catch (Exception ex) {
/*  41 */       ex.printStackTrace();
/*     */     }
/*  43 */     return name;
/*     */   }
/*     */   
/*     */   public static String getUUIDFromName(String name) {
/*  47 */     try { ProfileData profC = new ProfileData(name);
/*  48 */       String UUID = null;
/*  49 */       int i = 1;
/*  50 */       PlayerProfile[] result = post(new URL("https://api.mojang.com/profiles/page/" + i), Proxy.NO_PROXY, gson.toJson(profC).getBytes());
/*  51 */       if (result.length == 0) {
/*  52 */         return null;
/*     */       }
/*  54 */       UUID = result[0].getId();
/*  55 */       return UUID.replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5");
/*     */ 
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/*  60 */       e.printStackTrace();
/*     */     }
/*  62 */     return null;
/*     */   }
/*     */   
/*  65 */   public static UUID getSpoofedUUIDFromName(String name) { return UUID.nameUUIDFromBytes(("OfflinePlayer:" + name).getBytes(Charsets.UTF_8)); }
/*     */   
/*     */   private static PlayerProfile[] post(URL url, Proxy proxy, byte[] bytes) throws IOException {
/*  68 */     HttpURLConnection connection = (HttpURLConnection)url.openConnection(proxy);
/*  69 */     connection.setRequestMethod("POST");
/*  70 */     connection.setRequestProperty("Content-Type", "application/json");
/*  71 */     connection.setDoInput(true);
/*  72 */     connection.setDoOutput(true);
/*  73 */     DataOutputStream out = new DataOutputStream(connection.getOutputStream());
/*  74 */     out.write(bytes);
/*  75 */     out.flush();
/*  76 */     out.close();
/*  77 */     BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
/*  78 */     StringBuffer response = new StringBuffer();
/*     */     String line;
/*  80 */     while ((line = reader.readLine()) != null) {
/*  81 */       response.append(line);
/*  82 */       response.append('\r');
/*     */     }
/*  84 */     reader.close();
/*  85 */     return ((SearchResult)gson.fromJson(response.toString(), SearchResult.class)).getProfiles();
/*     */   }
/*     */   
/*     */   private static class PlayerProfile { private String id;
/*     */     
/*  90 */     public String getId() { return this.id; }
/*     */   }
/*     */   
/*     */   private static class SearchResult {
/*     */     private UUIDLibrary.PlayerProfile[] profiles;
/*     */     
/*  96 */     public UUIDLibrary.PlayerProfile[] getProfiles() { return this.profiles; }
/*     */   }
/*     */   
/*     */   private static class ProfileData
/*     */   {
/*     */     private String name;
/* 102 */     private String agent = "minecraft";
/*     */     
/*     */     public ProfileData(String name) {
/* 105 */       this.name = name;
/*     */     }
/*     */   }
/*     */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/utils/UUIDLibrary.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */