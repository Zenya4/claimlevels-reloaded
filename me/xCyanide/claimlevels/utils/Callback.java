package me.xCyanide.claimlevels.utils;

public abstract interface Callback<T>
{
  public abstract void done(T paramT);
}


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/utils/Callback.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */