/*    */ package me.xCyanide.claimlevels;
/*    */ 
/*    */ import java.io.File;
/*    */ import me.xCyanide.claimlevels.cmds.AddCreditsCommand;
/*    */ import me.xCyanide.claimlevels.cmds.CreditsCommand;
/*    */ import me.xCyanide.claimlevels.cmds.RedeemCommand;
/*    */ import me.xCyanide.claimlevels.cmds.TakeCreditsCommand;
/*    */ import me.xCyanide.claimlevels.io.Config;
/*    */ import me.xCyanide.claimlevels.io.FileManager;
/*    */ import me.xCyanide.claimlevels.io.Lang;
/*    */ import me.xCyanide.claimlevels.listeners.GeneralListener;
/*    */ import me.xCyanide.claimlevels.structure.PlayerManager;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.Server;
/*    */ import org.bukkit.command.PluginCommand;
/*    */ import org.bukkit.configuration.file.YamlConfiguration;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.plugin.PluginManager;
/*    */ import org.bukkit.plugin.java.JavaPlugin;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ClaimLevels
/*    */   extends JavaPlugin
/*    */ {
/*    */   private static ClaimLevels instance;
/* 27 */   private static int configVersion = 1;
/* 28 */   public static final String[] SKILLS = { "Mining", "Woodcutting", "Herbalism", "Fishing", "Excavation", "Unarmed", "Archery", "Swords", "Axes", "Taming", "Repair", "Acrobatics", "Alchemy" };
/*    */   
/*    */   public void onEnable() {
/* 31 */     instance = this;
/*    */     
/* 33 */     saveDefaultConfig();
/*    */     
/* 35 */     File configFile = new File(getDataFolder(), "config.yml");
/* 36 */     YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
/*    */     
/*    */ 
/* 39 */     if (config.getInt("ConfigVersion") != configVersion) {
/* 40 */       configFile.delete();
/* 41 */       saveDefaultConfig();
/*    */     }
/*    */     
/* 44 */     FileManager.load();
/* 45 */     Config.load();
/* 46 */     Lang.load();
/*    */     
/* 48 */     getServer().getPluginManager().registerEvents(new GeneralListener(), this);
/* 49 */     getCommand("credits").setExecutor(new CreditsCommand());
/* 50 */     getCommand("redeem").setExecutor(new RedeemCommand());
/* 51 */     getCommand("addcredits").setExecutor(new AddCreditsCommand());
/* 52 */     getCommand("takecredits").setExecutor(new TakeCreditsCommand());
/*    */     
/* 54 */     for (Player player : Bukkit.getOnlinePlayers()) {
/* 55 */       PlayerManager.loadPlayer(player);
/*    */     }
/*    */   }
/*    */   
/*    */   public void onDisable() {
/* 60 */     instance = null;
/*    */   }
/*    */   
/*    */   public static ClaimLevels getInstance() {
/* 64 */     return instance;
/*    */   }
/*    */   
/*    */   public static boolean isSkill(String s) {
/* 68 */     for (String skill : SKILLS) {
/* 69 */       if (skill.equalsIgnoreCase(s)) {
/* 70 */         return true;
/*    */       }
/*    */     }
/* 73 */     return false;
/*    */   }
/*    */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/ClaimLevels.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */