/*     */ package me.xCyanide.claimlevels.cmds;
/*     */ 
/*     */ import java.util.Map;
/*     */ import java.util.UUID;
/*     */ import me.xCyanide.claimlevels.io.FileManager;
/*     */ import me.xCyanide.claimlevels.io.Lang;
/*     */ import me.xCyanide.claimlevels.structure.CPlayer;
/*     */ import me.xCyanide.claimlevels.structure.PlayerManager;
/*     */ import me.xCyanide.claimlevels.utils.Callback;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.ChatColor;
/*     */ import org.bukkit.OfflinePlayer;
/*     */ import org.bukkit.command.Command;
/*     */ import org.bukkit.command.CommandExecutor;
/*     */ import org.bukkit.command.CommandSender;
/*     */ import org.bukkit.configuration.file.FileConfiguration;
/*     */ import org.bukkit.entity.Player;
/*     */ 
/*     */ 
/*     */ public class AddCreditsCommand
/*     */   implements CommandExecutor
/*     */ {
/*     */   public boolean onCommand(final CommandSender sender, Command cmd, String label, final String[] args)
/*     */   {
/*  25 */     if (sender.hasPermission("claimlevels.addcredits")) {
/*  26 */       if (args.length == 2) {
/*  27 */         final String playerName = args[0];
/*     */         
/*  29 */         Player target = Bukkit.getPlayer(playerName);
/*     */         
/*  31 */         if (target != null) {
/*  32 */           CPlayer cPlayer = (CPlayer)PlayerManager.getPlayerMap().get(target.getUniqueId().toString());
/*     */           try
/*     */           {
/*  35 */             Integer.parseInt(args[1]);
/*     */           } catch (NumberFormatException e) {
/*  37 */             sender.sendMessage(Lang.getPrefix() + Lang.getInvalidNumberMessage());
/*  38 */             return true;
/*     */           }
/*     */           
/*  41 */           int amount = Integer.parseInt(args[1]);
/*     */           
/*  43 */           if (amount <= 0) {
/*  44 */             sender.sendMessage(Lang.getPrefix() + Lang.getInvalidNumberMessage());
/*  45 */             return true;
/*     */           }
/*     */           
/*  48 */           cPlayer.addCredits(amount);
/*  49 */           sender.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "You gave " + ChatColor.AQUA + target.getName() + " " + ChatColor.GOLD + ChatColor.UNDERLINE + amount + ChatColor.GREEN + " credits");
/*  50 */           target.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "You received " + ChatColor.GOLD + ChatColor.UNDERLINE + amount + ChatColor.GREEN + " credits. Use " + ChatColor.YELLOW + "/redeem" + ChatColor.GREEN + " to redeem them");
/*  51 */           return true;
/*     */         }
/*  53 */         PlayerManager.getUUIDAsync(playerName, new Callback()
/*     */         {
/*     */           public void done(String uuid) {
/*  56 */             if (uuid == null) {
/*  57 */               sender.sendMessage(Lang.getPrefix() + Lang.getNoPlayerFoundMessage());
/*  58 */               return;
/*     */             }
/*     */             
/*  61 */             OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
/*     */             
/*  63 */             if (!offlinePlayer.hasPlayedBefore()) {
/*  64 */               sender.sendMessage(Lang.getPrefix() + Lang.getNoPlayerFoundMessage());
/*  65 */               return;
/*     */             }
/*     */             try
/*     */             {
/*  69 */               Integer.parseInt(args[1]);
/*     */             } catch (NumberFormatException e) {
/*  71 */               sender.sendMessage(Lang.getPrefix() + Lang.getInvalidNumberMessage());
/*  72 */               return;
/*     */             }
/*     */             
/*  75 */             int currentCredits = 0;
/*     */             
/*  77 */             if (FileManager.getDataConfig().contains(uuid)) {
/*  78 */               currentCredits = FileManager.getDataConfig().getInt(uuid + ".Credits");
/*     */             }
/*     */             
/*  81 */             int amount = Integer.parseInt(args[1]);
/*     */             
/*  83 */             if (amount <= 0) {
/*  84 */               sender.sendMessage(Lang.getPrefix() + Lang.getInvalidNumberMessage());
/*  85 */               return;
/*     */             }
/*     */             
/*  88 */             FileManager.getDataConfig().set(uuid + ".Credits", Integer.valueOf(amount + currentCredits));
/*  89 */             FileManager.saveDataConfig();
/*  90 */             sender.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "You gave " + ChatColor.AQUA + playerName + " " + ChatColor.GOLD + ChatColor.UNDERLINE + amount + ChatColor.GREEN + " credits");
/*     */           }
/*     */         });
/*     */       }
/*     */       else {
/*  95 */         sender.sendMessage(Lang.getPrefix() + ChatColor.YELLOW + "/addcredits [player] [amount]");
/*     */       }
/*     */     } else {
/*  98 */       sender.sendMessage(Lang.getPrefix() + Lang.getNoPermissionMessage());
/*     */     }
/* 100 */     return false;
/*     */   }
/*     */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/cmds/AddCreditsCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */