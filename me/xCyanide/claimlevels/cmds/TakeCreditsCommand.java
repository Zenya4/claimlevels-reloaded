/*     */ package me.xCyanide.claimlevels.cmds;
/*     */ 
/*     */ import java.util.Map;
/*     */ import java.util.UUID;
/*     */ import me.xCyanide.claimlevels.io.FileManager;
/*     */ import me.xCyanide.claimlevels.io.Lang;
/*     */ import me.xCyanide.claimlevels.structure.CPlayer;
/*     */ import me.xCyanide.claimlevels.structure.PlayerManager;
/*     */ import me.xCyanide.claimlevels.utils.Callback;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.ChatColor;
/*     */ import org.bukkit.OfflinePlayer;
/*     */ import org.bukkit.command.Command;
/*     */ import org.bukkit.command.CommandExecutor;
/*     */ import org.bukkit.command.CommandSender;
/*     */ import org.bukkit.configuration.file.FileConfiguration;
/*     */ import org.bukkit.entity.Player;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TakeCreditsCommand
/*     */   implements CommandExecutor
/*     */ {
/*     */   public boolean onCommand(final CommandSender sender, Command cmd, String label, final String[] args)
/*     */   {
/*  26 */     if (sender.hasPermission("claimlevels.takecredits")) {
/*  27 */       if (args.length == 2) {
/*  28 */         final String playerName = args[0];
/*     */         
/*  30 */         Player target = Bukkit.getPlayer(playerName);
/*     */         
/*  32 */         if (target != null) {
/*  33 */           CPlayer cPlayer = (CPlayer)PlayerManager.getPlayerMap().get(target.getUniqueId().toString());
/*     */           try
/*     */           {
/*  36 */             Integer.parseInt(args[1]);
/*     */           } catch (NumberFormatException e) {
/*  38 */             sender.sendMessage(Lang.getPrefix() + Lang.getInvalidNumberMessage());
/*  39 */             return true;
/*     */           }
/*     */           
/*  42 */           int amount = Integer.parseInt(args[1]);
/*     */           
/*  44 */           if (amount <= 0) {
/*  45 */             sender.sendMessage(Lang.getPrefix() + Lang.getInvalidNumberMessage());
/*  46 */             return true;
/*     */           }
/*     */           
/*  49 */           cPlayer.removeCredits(amount);
/*  50 */           sender.sendMessage(Lang.getPrefix() + ChatColor.RED + "You took " + ChatColor.GOLD + ChatColor.UNDERLINE + amount + ChatColor.RED + " credits from " + ChatColor.AQUA + target.getName());
/*  51 */           return true;
/*     */         }
/*  53 */         PlayerManager.getUUIDAsync(playerName, new Callback()
/*     */         {
/*     */           public void done(String uuid) {
/*  56 */             if (uuid == null) {
/*  57 */               sender.sendMessage(Lang.getPrefix() + Lang.getNoPlayerFoundMessage());
/*  58 */               return;
/*     */             }
/*     */             
/*  61 */             OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
/*     */             
/*  63 */             if (!offlinePlayer.hasPlayedBefore()) {
/*  64 */               sender.sendMessage(Lang.getPrefix() + Lang.getNoPlayerFoundMessage());
/*  65 */               return;
/*     */             }
/*     */             try
/*     */             {
/*  69 */               Integer.parseInt(args[1]);
/*     */             } catch (NumberFormatException e) {
/*  71 */               sender.sendMessage(Lang.getPrefix() + Lang.getInvalidNumberMessage());
/*  72 */               return;
/*     */             }
/*     */             
/*  75 */             int currentCredits = 0;
/*     */             
/*  77 */             if (FileManager.getDataConfig().contains(uuid)) {
/*  78 */               currentCredits = FileManager.getDataConfig().getInt(uuid + ".Credits");
/*     */             }
/*     */             
/*  81 */             int amount = Integer.parseInt(args[1]);
/*     */             
/*  83 */             if (amount <= 0) {
/*  84 */               sender.sendMessage(Lang.getPrefix() + Lang.getInvalidNumberMessage());
/*  85 */               return;
/*     */             }
/*     */             
/*  88 */             if ((currentCredits > 0) && (currentCredits - amount >= 0)) {
/*  89 */               FileManager.getDataConfig().set(uuid + ".Credits", Integer.valueOf(currentCredits - amount));
/*  90 */               FileManager.saveDataConfig();
/*  91 */               sender.sendMessage(Lang.getPrefix() + ChatColor.RED + "You took " + ChatColor.GOLD + ChatColor.UNDERLINE + amount + ChatColor.RED + " credits from " + ChatColor.AQUA + playerName);
/*     */             } else {
/*  93 */               sender.sendMessage(Lang.getPrefix() + ChatColor.RED + "Players cannot have negative credits");
/*     */             }
/*     */           }
/*     */         });
/*     */       }
/*     */       else {
/*  99 */         sender.sendMessage(Lang.getPrefix() + ChatColor.YELLOW + "/addcredits [player] [amount]");
/*     */       }
/*     */     } else {
/* 102 */       sender.sendMessage(Lang.getPrefix() + Lang.getNoPermissionMessage());
/*     */     }
/* 104 */     return false;
/*     */   }
/*     */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/cmds/TakeCreditsCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */