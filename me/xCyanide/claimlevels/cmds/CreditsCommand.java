/*     */ package me.xCyanide.claimlevels.cmds;
/*     */ 
/*     */ import java.util.Map;
/*     */ import java.util.UUID;
/*     */ import me.xCyanide.claimlevels.io.Lang;
/*     */ import me.xCyanide.claimlevels.structure.CPlayer;
/*     */ import me.xCyanide.claimlevels.structure.PlayerManager;
/*     */ import me.xCyanide.claimlevels.utils.Callback;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.ChatColor;
/*     */ import org.bukkit.command.Command;
/*     */ import org.bukkit.command.CommandExecutor;
/*     */ import org.bukkit.command.CommandSender;
/*     */ import org.bukkit.entity.Player;
/*     */ 
/*     */ 
/*     */ public class CreditsCommand
/*     */   implements CommandExecutor
/*     */ {
/*     */   public boolean onCommand(final CommandSender sender, Command cmd, String label, String[] args)
/*     */   {
/*  22 */     if ((args.length > 0) && (args[0].equalsIgnoreCase("pay"))) {
/*  23 */       if ((sender instanceof Player)) {
/*  24 */         Player player = (Player)sender;
/*  25 */         if (args.length == 3) {
/*  26 */           int credits = ((CPlayer)PlayerManager.getPlayerMap().get(player.getUniqueId().toString())).getCredits();
/*     */           
/*  28 */           String targetName = args[1];
/*     */           
/*  30 */           if (targetName.equalsIgnoreCase(player.getName())) {
/*  31 */             player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You cannot send credits to yourself!");
/*  32 */             return true;
/*     */           }
/*     */           
/*  35 */           if (Bukkit.getPlayer(targetName) != null) {
/*  36 */             Player target = Bukkit.getPlayer(targetName);
/*     */             try
/*     */             {
/*  39 */               Integer.parseInt(args[2]);
/*     */             } catch (NumberFormatException e) {
/*  41 */               player.sendMessage(Lang.getPrefix() + ChatColor.RED + Lang.getInvalidNumberMessage());
/*  42 */               return true;
/*     */             }
/*     */             
/*  45 */             int amount = Integer.parseInt(args[2]);
/*     */             
/*  47 */             if (amount <= 0) {
/*  48 */               player.sendMessage(Lang.getPrefix() + ChatColor.RED + Lang.getInvalidNumberMessage());
/*  49 */               return true;
/*     */             }
/*     */             
/*  52 */             if (credits >= amount) {
/*  53 */               CPlayer creditSender = (CPlayer)PlayerManager.getPlayerMap().get(player.getUniqueId().toString());
/*  54 */               creditSender.removeCredits(amount);
/*  55 */               CPlayer cPlayer = (CPlayer)PlayerManager.getPlayerMap().get(target.getUniqueId().toString());
/*  56 */               cPlayer.addCredits(amount);
/*  57 */               player.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "You sent " + ChatColor.YELLOW + ChatColor.UNDERLINE + amount + ChatColor.GREEN + " credits to " + ChatColor.AQUA + target.getName());
/*  58 */               target.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "You received " + ChatColor.YELLOW + ChatColor.UNDERLINE + amount + ChatColor.GREEN + " credits from " + ChatColor.AQUA + player.getName());
/*     */             } else {
/*  60 */               player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have enough credits");
/*     */             }
/*     */           } else {
/*  63 */             player.sendMessage(Lang.getPrefix() + Lang.getNoPlayerFoundMessage());
/*     */           }
/*     */         } else {
/*  66 */           player.sendMessage(Lang.getPrefix() + ChatColor.RED + "/credits pay [name] [amount]");
/*     */         }
/*     */       } else {
/*  69 */         sender.sendMessage(Lang.getPlayerOnlyMessage());
/*     */       }
/*  71 */       return true;
/*     */     }
/*     */     
/*  74 */     if (args.length == 0) {
/*  75 */       if ((sender instanceof Player)) {
/*  76 */         Player player = (Player)sender;
/*  77 */         int credits = ((CPlayer)PlayerManager.getPlayerMap().get(player.getUniqueId().toString())).getCredits();
/*  78 */         player.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "----------------------------------------");
/*  79 */         player.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "You have " + ChatColor.GOLD + "" + ChatColor.UNDERLINE + credits + ChatColor.GREEN + " credits");
/*  80 */         player.sendMessage(ChatColor.GREEN + "/credits pay [name] [amount] " + ChatColor.WHITE + "-" + ChatColor.YELLOW + " Send credits to other players");
/*  81 */         player.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "----------------------------------------");
/*     */       } else {
/*  83 */         sender.sendMessage(Lang.getPlayerOnlyMessage());
/*     */       }
/*  85 */     } else if (args.length == 1) {
/*  86 */       if (sender.hasPermission("claimlevels.credits.others")) {
/*  87 */         final String name = args[0];
/*  88 */         PlayerManager.getCreditsAsync(name, new Callback()
/*     */         {
/*     */           public void done(Integer integer) {
/*  91 */             if (integer.intValue() == -1) {
/*  92 */               sender.sendMessage(Lang.getPrefix() + Lang.getNoPlayerFoundMessage());
/*     */             } else {
/*  94 */               sender.sendMessage(Lang.getPrefix() + ChatColor.GREEN + name + " has " + ChatColor.GOLD + "" + ChatColor.UNDERLINE + integer + ChatColor.GREEN + " credits");
/*     */             }
/*     */           }
/*     */         });
/*     */       } else {
/*  99 */         sender.sendMessage(Lang.getPrefix() + Lang.getNoPermissionMessage());
/*     */       }
/*     */     } else {
/* 102 */       sender.sendMessage(Lang.getPrefix() + ChatColor.YELLOW + "/credits <player>");
/*     */     }
/* 104 */     return false;
/*     */   }
/*     */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/cmds/CreditsCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */