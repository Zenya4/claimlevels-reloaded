/*    */ package me.xCyanide.claimlevels.cmds;
/*    */ 
/*    */ import com.gmail.nossr50.api.ExperienceAPI;
/*    */ import java.util.Map;
/*    */ import java.util.UUID;
/*    */ import me.xCyanide.claimlevels.ClaimLevels;
/*    */ import me.xCyanide.claimlevels.gui.RedeemGUI;
/*    */ import me.xCyanide.claimlevels.io.Config;
/*    */ import me.xCyanide.claimlevels.io.Lang;
/*    */ import me.xCyanide.claimlevels.structure.CPlayer;
/*    */ import me.xCyanide.claimlevels.structure.PlayerManager;
/*    */ import org.bukkit.ChatColor;
/*    */ import org.bukkit.command.Command;
/*    */ import org.bukkit.command.CommandExecutor;
/*    */ import org.bukkit.command.CommandSender;
/*    */ import org.bukkit.entity.Player;
/*    */ 
/*    */ public class RedeemCommand
/*    */   implements CommandExecutor
/*    */ {
/*    */   public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
/*    */   {
/* 23 */     if ((sender instanceof Player)) {
/* 24 */       Player player = (Player)sender;
/* 25 */       if (!Config.enableGUI()) {
/* 26 */         if (args.length == 2) {
/* 27 */           String skill = args[0];
/* 28 */           if (!ClaimLevels.isSkill(skill)) {
/* 29 */             player.sendMessage(Lang.getPrefix() + ChatColor.RED + "That is not a valid skill");
/* 30 */             return true;
/*    */           }
/*    */           try
/*    */           {
/* 34 */             Integer.parseInt(args[1]);
/*    */           } catch (NumberFormatException e) {
/* 36 */             player.sendMessage(Lang.getPrefix() + ChatColor.RED + Lang.getInvalidNumberMessage());
/* 37 */             return true;
/*    */           }
/*    */           
/* 40 */           int amount = Integer.parseInt(args[1]);
/*    */           
/* 42 */           if (amount <= 0) {
/* 43 */             player.sendMessage(Lang.getPrefix() + ChatColor.RED + Lang.getInvalidNumberMessage());
/* 44 */             return true;
/*    */           }
/*    */           
/* 47 */           CPlayer cPlayer = (CPlayer)PlayerManager.getPlayerMap().get(player.getUniqueId().toString());
/*    */           
/* 49 */           if (cPlayer.getCredits() < amount) {
/* 50 */             player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do " + ChatColor.UNDERLINE + "NOT" + ChatColor.RED + " have enough credits");
/* 51 */             return true;
/*    */           }
/*    */           
/* 54 */           if (amount + ExperienceAPI.getLevel(player, skill) > ExperienceAPI.getLevelCap(skill)) {
/* 55 */             player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You " + ChatColor.UNDERLINE + "CANNOT" + ChatColor.RED + " go over level cap of a skill");
/* 56 */             player.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "LEVEL CAP: " + ChatColor.GOLD + ExperienceAPI.getLevelCap(skill));
/* 57 */             return true;
/*    */           }
/*    */           
/* 60 */           cPlayer.removeCredits(amount);
/* 61 */           ExperienceAPI.addLevel(player, skill, amount);
/* 62 */           player.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "You successfully added " + ChatColor.GOLD + "" + ChatColor.BOLD + amount + ChatColor.GREEN + "" + ChatColor.BOLD + " levels to " + ChatColor.YELLOW + ChatColor.UNDERLINE + skill.toUpperCase());
/*    */         } else {
/* 64 */           player.sendMessage(Lang.getPrefix() + ChatColor.RED + "/redeem [skill] [amount]");
/*    */         }
/*    */       } else {
/* 67 */         RedeemGUI redeemGUI = new RedeemGUI(player);
/* 68 */         PlayerManager.getOpenGUIs().put(player.getUniqueId().toString(), redeemGUI);
/* 69 */         redeemGUI.open();
/*    */       }
/*    */     } else {
/* 72 */       sender.sendMessage(Lang.getPlayerOnlyMessage());
/*    */     }
/* 74 */     return false;
/*    */   }
/*    */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/cmds/RedeemCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */