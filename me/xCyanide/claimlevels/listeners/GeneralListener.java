/*    */ package me.xCyanide.claimlevels.listeners;
/*    */ 
/*    */ import java.util.Map;
/*    */ import java.util.UUID;
/*    */ import me.xCyanide.claimlevels.gui.RedeemGUI;
/*    */ import me.xCyanide.claimlevels.io.Lang;
/*    */ import me.xCyanide.claimlevels.structure.CPlayer;
/*    */ import me.xCyanide.claimlevels.structure.MessageResult;
/*    */ import me.xCyanide.claimlevels.structure.PlayerManager;
/*    */ import org.bukkit.ChatColor;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.EventPriority;
/*    */ import org.bukkit.event.inventory.InventoryClickEvent;
/*    */ import org.bukkit.event.inventory.InventoryCloseEvent;
/*    */ import org.bukkit.event.player.AsyncPlayerChatEvent;
/*    */ import org.bukkit.event.player.PlayerJoinEvent;
/*    */ import org.bukkit.event.player.PlayerQuitEvent;
/*    */ import org.bukkit.inventory.Inventory;
/*    */ 
/*    */ public class GeneralListener implements org.bukkit.event.Listener
/*    */ {
/*    */   @EventHandler(priority=EventPriority.MONITOR)
/*    */   public void onJoin(PlayerJoinEvent event)
/*    */   {
/* 26 */     Player player = event.getPlayer();
/* 27 */     PlayerManager.loadPlayer(player);
/*    */     
/* 29 */     if (me.xCyanide.claimlevels.io.Config.enableJoinMessage()) {
/* 30 */       player.sendMessage(Lang.getPrefix() + ChatColor.YELLOW + "This server is running " + ChatColor.GOLD + "" + ChatColor.BOLD + "CLAIMLEVELS v0.1");
/* 31 */       player.sendMessage(Lang.getPrefix() + ChatColor.YELLOW + "Author: xCyanide");
/* 32 */       player.sendMessage("");
/* 33 */       int credits = ((CPlayer)PlayerManager.getPlayerMap().get(player.getUniqueId().toString())).getCredits();
/* 34 */       player.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "You have " + ChatColor.GOLD + "" + ChatColor.UNDERLINE + credits + ChatColor.GREEN + " credits");
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   @EventHandler(priority=EventPriority.MONITOR)
/*    */   public void onQuit(PlayerQuitEvent event)
/*    */   {
/* 42 */     Player player = event.getPlayer();
/* 43 */     PlayerManager.unloadPlayer(player);
/*    */   }
/*    */   
/*    */   @EventHandler(priority=EventPriority.LOWEST)
/*    */   public void onChat(AsyncPlayerChatEvent event) {
/* 48 */     Player player = event.getPlayer();
/* 49 */     if (PlayerManager.getMessageMap().containsKey(player.getUniqueId().toString())) {
/* 50 */       MessageResult mr = (MessageResult)PlayerManager.getMessageMap().get(player.getUniqueId().toString());
/* 51 */       mr.messageRecieve(event.getMessage());
/* 52 */       event.setCancelled(true);
/* 53 */       return;
/*    */     }
/*    */   }
/*    */   
/*    */   @EventHandler
/*    */   public void onInventoryClick(InventoryClickEvent event) {
/* 59 */     if (event.getInventory().getTitle().equalsIgnoreCase("Redeem Menu")) {
/* 60 */       Player player = (Player)event.getWhoClicked();
/* 61 */       ((RedeemGUI)PlayerManager.getOpenGUIs().get(player.getUniqueId().toString())).onClick(event);
/*    */     }
/*    */   }
/*    */   
/*    */   @EventHandler
/*    */   public void onInventoryClose(InventoryCloseEvent event) {
/* 67 */     if (event.getInventory().getTitle().equalsIgnoreCase("Redeem Menu")) {
/* 68 */       Player player = (Player)event.getPlayer();
/* 69 */       PlayerManager.getOpenGUIs().remove(player.getUniqueId().toString());
/*    */     }
/*    */   }
/*    */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/listeners/GeneralListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */