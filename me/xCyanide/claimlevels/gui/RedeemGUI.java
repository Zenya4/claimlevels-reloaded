/*     */ package me.xCyanide.claimlevels.gui;
/*     */ 
/*     */ import java.util.Map;
/*     */ import java.util.UUID;
/*     */ import me.xCyanide.claimlevels.io.Lang;
/*     */ import me.xCyanide.claimlevels.structure.CPlayer;
/*     */ import me.xCyanide.claimlevels.structure.MessageResult;
/*     */ import me.xCyanide.claimlevels.structure.PlayerManager;
/*     */ import me.xCyanide.claimlevels.utils.ItemUtils;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.ChatColor;
/*     */ import org.bukkit.Material;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.event.inventory.InventoryClickEvent;
/*     */ import org.bukkit.inventory.Inventory;
/*     */ import org.bukkit.inventory.ItemStack;
/*     */ import org.bukkit.inventory.meta.ItemMeta;
/*     */ 
/*     */ public class RedeemGUI
/*     */ {
/*     */   private Inventory inv;
/*     */   private Player player;
/*     */   
/*     */   public RedeemGUI(Player player)
/*     */   {
/*  26 */     this.player = player;
/*  27 */     this.inv = Bukkit.createInventory(player, 27, "Redeem Menu");
/*  28 */     loadInventoryContents();
/*     */   }
/*     */   
/*     */   public void open() {
/*  32 */     this.player.openInventory(this.inv);
/*     */   }
/*     */   
/*     */   public void onClick(InventoryClickEvent event)
/*     */   {
/*  37 */     ItemStack item = event.getCurrentItem();
/*     */     
/*  39 */     if (item == null) {
/*  40 */       return;
/*     */     }
/*     */     
/*     */ 
/*  44 */     if (item.getType().equals(Material.DIAMOND_PICKAXE)) {
/*  45 */       if (this.player.hasPermission("mcmmo.skills.mining")) {
/*  46 */         this.player.closeInventory();
/*  47 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO MINING. TYPE CANCEL TO EXIT THIS");
/*  48 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Mining"));
/*     */       } else {
/*  50 */         this.player.closeInventory();
/*  51 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  56 */     if (item.getType().equals(Material.IRON_AXE)) {
/*  57 */       if (this.player.hasPermission("mcmmo.skills.woodcutting")) {
/*  58 */         this.player.closeInventory();
/*  59 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO WOODCUTTING. TYPE CANCEL TO EXIT THIS");
/*  60 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Woodcutting"));
/*     */       } else {
/*  62 */         this.player.closeInventory();
/*  63 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  68 */     if (item.getType().equals(Material.DIAMOND_HOE)) {
/*  69 */       if (this.player.hasPermission("mcmmo.skills.herbalism")) {
/*  70 */         this.player.closeInventory();
/*  71 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO HERBALISM. TYPE CANCEL TO EXIT THIS");
/*  72 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Herbalism"));
/*     */       } else {
/*  74 */         this.player.closeInventory();
/*  75 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  80 */     if (item.getType().equals(Material.FISHING_ROD)) {
/*  81 */       if (this.player.hasPermission("mcmmo.skills.fishing")) {
/*  82 */         this.player.closeInventory();
/*  83 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO FISHING. TYPE CANCEL TO EXIT THIS");
/*  84 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Fishing"));
/*     */       } else {
/*  86 */         this.player.closeInventory();
/*  87 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  92 */     if (item.getType().equals(Material.DIAMOND_SPADE)) {
/*  93 */       if (this.player.hasPermission("mcmmo.skills.excavation")) {
/*  94 */         this.player.closeInventory();
/*  95 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO EXCAVATION. TYPE CANCEL TO EXIT THIS");
/*  96 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Excavation"));
/*     */       } else {
/*  98 */         this.player.closeInventory();
/*  99 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 104 */     if (item.getType().equals(Material.STAINED_GLASS)) {
/* 105 */       if (this.player.hasPermission("mcmmo.skills.unarmed")) {
/* 106 */         this.player.closeInventory();
/* 107 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO UNARMED. TYPE CANCEL TO EXIT THIS");
/* 108 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Unarmed"));
/*     */       } else {
/* 110 */         this.player.closeInventory();
/* 111 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 116 */     if (item.getType().equals(Material.BOW)) {
/* 117 */       if (this.player.hasPermission("mcmmo.skills.archery")) {
/* 118 */         this.player.closeInventory();
/* 119 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO ARCHERY. TYPE CANCEL TO EXIT THIS");
/* 120 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Archery"));
/*     */       } else {
/* 122 */         this.player.closeInventory();
/* 123 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 128 */     if (item.getType().equals(Material.DIAMOND_SWORD)) {
/* 129 */       if (this.player.hasPermission("mcmmo.skills.swords")) {
/* 130 */         this.player.closeInventory();
/* 131 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO SWORDS. TYPE CANCEL TO EXIT THIS");
/* 132 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Swords"));
/*     */       } else {
/* 134 */         this.player.closeInventory();
/* 135 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 140 */     if (item.getType().equals(Material.DIAMOND_AXE)) {
/* 141 */       if (this.player.hasPermission("mcmmo.skills.axes")) {
/* 142 */         this.player.closeInventory();
/* 143 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO AXES. TYPE CANCEL TO EXIT THIS");
/* 144 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Axes"));
/*     */       } else {
/* 146 */         this.player.closeInventory();
/* 147 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 152 */     if (item.getType().equals(Material.BONE)) {
/* 153 */       if (this.player.hasPermission("mcmmo.skills.taming")) {
/* 154 */         this.player.closeInventory();
/* 155 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO BONE. TYPE CANCEL TO EXIT THIS");
/* 156 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Taming"));
/*     */       } else {
/* 158 */         this.player.closeInventory();
/* 159 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 164 */     if (item.getType().equals(Material.IRON_BLOCK)) {
/* 165 */       if (this.player.hasPermission("mcmmo.skills.repair")) {
/* 166 */         this.player.closeInventory();
/* 167 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO REPAIR. TYPE CANCEL TO EXIT THIS");
/* 168 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Repair"));
/*     */       } else {
/* 170 */         this.player.closeInventory();
/* 171 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 176 */     if (item.getType().equals(Material.DIAMOND_BOOTS)) {
/* 177 */       if (this.player.hasPermission("mcmmo.skills.acrobatics")) {
/* 178 */         this.player.closeInventory();
/* 179 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO ACROBATICS. TYPE CANCEL TO EXIT THIS");
/* 180 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Acrobatics"));
/*     */       } else {
/* 182 */         this.player.closeInventory();
/* 183 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 188 */     if (item.getType().equals(Material.POTION)) {
/* 189 */       if (this.player.hasPermission("mcmmo.skills.alchemy")) {
/* 190 */         this.player.closeInventory();
/* 191 */         this.player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "ENTER THE NUMBER OF LEVEL YOU WANT TO ADD TO ALCHEMY. TYPE CANCEL TO EXIT THIS");
/* 192 */         PlayerManager.getMessageMap().put(this.player.getUniqueId().toString(), new MessageResult(this.player, "Alchemy"));
/*     */       } else {
/* 194 */         this.player.closeInventory();
/* 195 */         this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do not have permission to use this skill");
/*     */       }
/*     */     }
/*     */     
/* 199 */     event.setCancelled(true);
/*     */   }
/*     */   
/*     */ 
/*     */   private void loadInventoryContents()
/*     */   {
/* 205 */     ItemStack mining = ItemUtils.signItemStack(new ItemStack(Material.DIAMOND_PICKAXE), ChatColor.AQUA + "" + ChatColor.BOLD + "MINING", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.AQUA + "" + ChatColor.BOLD + "MINING" });
/*     */     
/* 207 */     this.inv.setItem(1, mining);
/*     */     
/* 209 */     ItemStack woodcutting = ItemUtils.signItemStack(new ItemStack(Material.IRON_AXE), ChatColor.GOLD + "" + ChatColor.BOLD + "WOODCUTTING", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.AQUA + "" + ChatColor.GOLD + "" + ChatColor.BOLD + "WOODCUTTING" });
/*     */     
/* 211 */     this.inv.setItem(2, woodcutting);
/*     */     
/* 213 */     ItemStack herbalism = ItemUtils.signItemStack(new ItemStack(Material.DIAMOND_HOE), ChatColor.GREEN + "" + ChatColor.BOLD + "HERBALISM", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.GREEN + "" + ChatColor.BOLD + "HERBALISM" });
/*     */     
/* 215 */     this.inv.setItem(3, herbalism);
/*     */     
/* 217 */     ItemStack fishing = ItemUtils.signItemStack(new ItemStack(Material.FISHING_ROD), ChatColor.BLUE + "" + ChatColor.BOLD + "FISHING", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.BLUE + "" + ChatColor.BOLD + "FISHING" });
/*     */     
/* 219 */     this.inv.setItem(4, fishing);
/*     */     
/* 221 */     ItemStack excavation = ItemUtils.signItemStack(new ItemStack(Material.DIAMOND_SPADE), ChatColor.GRAY + "" + ChatColor.BOLD + "EXCAVATION", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.GRAY + "" + ChatColor.BOLD + "EXCAVATION" });
/*     */     
/* 223 */     this.inv.setItem(5, excavation);
/*     */     
/* 225 */     ItemStack unarmed = ItemUtils.signItemStack(new ItemStack(Material.STAINED_GLASS), ChatColor.WHITE + "" + ChatColor.BOLD + "UNARMED", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.WHITE + "" + ChatColor.BOLD + "UNARMED" });
/*     */     
/* 227 */     this.inv.setItem(6, unarmed);
/*     */     
/* 229 */     ItemStack archery = ItemUtils.signItemStack(new ItemStack(Material.BOW), ChatColor.YELLOW + "" + ChatColor.BOLD + "ARCHERY", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.YELLOW + "" + ChatColor.BOLD + "ARCHERY" });
/*     */     
/* 231 */     this.inv.setItem(7, archery);
/*     */     
/* 233 */     ItemStack swords = ItemUtils.signItemStack(new ItemStack(Material.DIAMOND_SWORD), ChatColor.RED + "" + ChatColor.BOLD + "SWORDS", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.RED + "" + ChatColor.BOLD + "SWORDS" });
/*     */     
/* 235 */     this.inv.setItem(11, swords);
/*     */     
/* 237 */     ItemStack axes = ItemUtils.signItemStack(new ItemStack(Material.DIAMOND_AXE), ChatColor.DARK_RED + "" + ChatColor.BOLD + "AXES", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.DARK_RED + "" + ChatColor.BOLD + "AXES" });
/*     */     
/* 239 */     this.inv.setItem(12, axes);
/*     */     
/* 241 */     ItemStack taming = ItemUtils.signItemStack(new ItemStack(Material.BONE), ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "TAMING", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "TAMING" });
/*     */     
/* 243 */     this.inv.setItem(13, taming);
/*     */     
/* 245 */     ItemStack repair = ItemUtils.signItemStack(new ItemStack(Material.IRON_BLOCK), ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "REPAIR", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "REPAIR" });
/*     */     
/* 247 */     this.inv.setItem(14, repair);
/*     */     
/* 249 */     ItemStack acrobatics = ItemUtils.signItemStack(new ItemStack(Material.DIAMOND_BOOTS), ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "ACROBATICS", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "ACROBATICS" });
/*     */     
/* 251 */     this.inv.setItem(15, acrobatics);
/*     */     
/* 253 */     ItemStack alchemy = ItemUtils.signItemStack(new ItemStack(Material.POTION, 1, (short)45), ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "ALCHEMY", new String[] { ChatColor.GRAY + "Click to assign levels to " + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "ALCHEMY" });
/*     */     
/* 255 */     this.inv.setItem(22, alchemy);
/*     */     
/* 257 */     for (int i = 0; i < this.inv.getSize(); i++) {
/* 258 */       if (this.inv.getItem(i) == null) {
/* 259 */         ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)15);
/* 260 */         ItemMeta meta = item.getItemMeta();
/* 261 */         meta.setDisplayName(ChatColor.GRAY + "Credits: " + ChatColor.GOLD + "" + ChatColor.BOLD + ((CPlayer)PlayerManager.getPlayerMap().get(this.player.getUniqueId().toString())).getCredits());
/* 262 */         item.setItemMeta(meta);
/* 263 */         this.inv.setItem(i, item);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/gui/RedeemGUI.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */