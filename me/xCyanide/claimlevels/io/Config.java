/*    */ package me.xCyanide.claimlevels.io;
/*    */ 
/*    */ import me.xCyanide.claimlevels.ClaimLevels;
/*    */ import org.bukkit.configuration.file.FileConfiguration;
/*    */ 
/*    */ 
/*    */ public class Config
/*    */ {
/*    */   private static boolean enableJoinMessage;
/*    */   private static boolean enableGUI;
/*    */   
/*    */   public static void load()
/*    */   {
/* 14 */     enableJoinMessage = ClaimLevels.getInstance().getConfig().getBoolean("JoinMessage");
/* 15 */     enableGUI = ClaimLevels.getInstance().getConfig().getBoolean("EnableGUI");
/*    */   }
/*    */   
/*    */   public static boolean enableJoinMessage() {
/* 19 */     return enableJoinMessage;
/*    */   }
/*    */   
/*    */   public static boolean enableGUI() {
/* 23 */     return enableGUI;
/*    */   }
/*    */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/io/Config.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */