/*    */ package me.xCyanide.claimlevels.io;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.IOException;
/*    */ import me.xCyanide.claimlevels.ClaimLevels;
/*    */ import org.bukkit.configuration.file.FileConfiguration;
/*    */ import org.bukkit.configuration.file.YamlConfiguration;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FileManager
/*    */ {
/* 15 */   private static File dataFile = new File(ClaimLevels.getInstance().getDataFolder(), "data.yml");
/*    */   
/* 17 */   private static File langFile = new File(ClaimLevels.getInstance().getDataFolder(), "language.yml");
/*    */   
/*    */   private static FileConfiguration dataConfig;
/*    */   
/*    */   private static FileConfiguration langConfig;
/*    */   
/* 23 */   private static boolean firstLoad = false;
/*    */   
/*    */   public static void load() {
/* 26 */     if (!dataFile.exists()) {
/*    */       try {
/* 28 */         dataFile.createNewFile();
/*    */       } catch (IOException e) {
/* 30 */         e.printStackTrace();
/*    */       }
/*    */     }
/*    */     
/* 34 */     dataConfig = YamlConfiguration.loadConfiguration(dataFile);
/*    */     
/* 36 */     if (!langFile.exists()) {
/*    */       try {
/* 38 */         langFile.createNewFile();
/*    */       } catch (IOException e) {
/* 40 */         e.printStackTrace();
/*    */       }
/* 42 */       firstLoad = true;
/*    */     }
/*    */     
/* 45 */     langConfig = YamlConfiguration.loadConfiguration(langFile);
/*    */     
/* 47 */     if (firstLoad) {
/* 48 */       langConfig.set("Prefix", "&6&lClaimLevels&8>&r ");
/* 49 */       langConfig.set("PlayerOnly", "&c&lTHAT IS A PLAYER ONLY COMMAND");
/* 50 */       langConfig.set("InvalidNumber", "&c&lINVALID NUMBER");
/* 51 */       langConfig.set("NoPermission", "&c&lYOU DO NOT HAVE ACCESS TO THIS COMMAND");
/* 52 */       langConfig.set("PlayerNotFound", "&c&lTHAT PLAYER DOES NOT EXIST OR HAS NOT JOINED THE SERVER");
/* 53 */       saveLangConfig();
/*    */     }
/*    */   }
/*    */   
/*    */   public static FileConfiguration getDataConfig() {
/* 58 */     return dataConfig;
/*    */   }
/*    */   
/*    */   public static void saveDataConfig() {
/*    */     try {
/* 63 */       dataConfig.save(dataFile);
/*    */     } catch (IOException e) {
/* 65 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */   
/*    */   public static FileConfiguration getLangConfig() {
/* 70 */     return langConfig;
/*    */   }
/*    */   
/*    */   public static void saveLangConfig() {
/*    */     try {
/* 75 */       langConfig.save(langFile);
/*    */     } catch (IOException e) {
/* 77 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/io/FileManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */