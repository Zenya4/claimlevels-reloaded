/*    */ package me.xCyanide.claimlevels.io;
/*    */ 
/*    */ import org.bukkit.ChatColor;
/*    */ import org.bukkit.configuration.file.FileConfiguration;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Lang
/*    */ {
/*    */   private static String prefix;
/*    */   private static String playerOnly;
/*    */   private static String invalidNumber;
/*    */   private static String noPermission;
/*    */   private static String noPlayerFound;
/*    */   
/*    */   public static void load()
/*    */   {
/* 18 */     FileConfiguration config = FileManager.getLangConfig();
/* 19 */     prefix = translateColorCode(config.getString("Prefix"));
/* 20 */     playerOnly = translateColorCode(config.getString("PlayerOnly"));
/* 21 */     invalidNumber = translateColorCode(config.getString("InvalidNumber"));
/* 22 */     noPermission = translateColorCode(config.getString("NoPermission"));
/* 23 */     noPlayerFound = translateColorCode(config.getString("PlayerNotFound"));
/*    */   }
/*    */   
/*    */   public static String getPrefix() {
/* 27 */     return prefix;
/*    */   }
/*    */   
/*    */   public static String getPlayerOnlyMessage() {
/* 31 */     return playerOnly;
/*    */   }
/*    */   
/*    */   public static String getInvalidNumberMessage() {
/* 35 */     return invalidNumber;
/*    */   }
/*    */   
/*    */   public static String getNoPermissionMessage() {
/* 39 */     return noPermission;
/*    */   }
/*    */   
/*    */   public static String getNoPlayerFoundMessage() {
/* 43 */     return noPlayerFound;
/*    */   }
/*    */   
/*    */   public static String translateColorCode(String msg) {
/* 47 */     return ChatColor.translateAlternateColorCodes('&', msg);
/*    */   }
/*    */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/io/Lang.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */