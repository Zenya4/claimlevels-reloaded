/*    */ package me.xCyanide.claimlevels.structure;
/*    */ 
/*    */ import com.gmail.nossr50.api.ExperienceAPI;
/*    */ import java.util.Map;
/*    */ import java.util.UUID;
/*    */ import me.xCyanide.claimlevels.io.Lang;
/*    */ import org.bukkit.ChatColor;
/*    */ import org.bukkit.entity.Player;
/*    */ 
/*    */ public class MessageResult
/*    */ {
/*    */   private Player player;
/*    */   private String skill;
/*    */   
/*    */   public MessageResult(Player player, String skill)
/*    */   {
/* 17 */     this.player = player;
/* 18 */     this.skill = skill;
/*    */   }
/*    */   
/*    */   public void messageRecieve(String message) {
/* 22 */     if (message.equalsIgnoreCase("CANCEL")) {
/* 23 */       this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "" + ChatColor.BOLD + "CANCELLING");
/* 24 */       PlayerManager.getMessageMap().remove(this.player.getUniqueId().toString());
/* 25 */       return;
/*    */     }
/*    */     try
/*    */     {
/* 29 */       Integer.parseInt(message);
/*    */     } catch (NumberFormatException e) {
/* 31 */       this.player.sendMessage(Lang.getPrefix() + Lang.getInvalidNumberMessage());
/* 32 */       PlayerManager.getMessageMap().remove(this.player.getUniqueId().toString());
/* 33 */       return;
/*    */     }
/*    */     
/* 36 */     CPlayer cPlayer = (CPlayer)PlayerManager.getPlayerMap().get(this.player.getUniqueId().toString());
/*    */     
/* 38 */     int amount = Integer.parseInt(message);
/*    */     
/* 40 */     if (amount <= 0) {
/* 41 */       this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + Lang.getInvalidNumberMessage());
/*    */     }
/* 43 */     else if (amount + ExperienceAPI.getLevel(this.player, this.skill) > ExperienceAPI.getLevelCap(this.skill)) {
/* 44 */       this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You " + ChatColor.UNDERLINE + "CANNOT" + ChatColor.RED + " go over level cap of a skill");
/* 45 */       this.player.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "LEVEL CAP: " + ChatColor.GOLD + ExperienceAPI.getLevelCap(this.skill));
/*    */     }
/* 47 */     else if (cPlayer.getCredits() >= amount) {
/* 48 */       cPlayer.removeCredits(amount);
/* 49 */       ExperienceAPI.addLevel(this.player, this.skill, amount);
/* 50 */       this.player.sendMessage(Lang.getPrefix() + ChatColor.GREEN + "You successfully added " + ChatColor.GOLD + "" + ChatColor.BOLD + amount + ChatColor.GREEN + "" + ChatColor.BOLD + " levels to " + ChatColor.YELLOW + ChatColor.UNDERLINE + this.skill.toUpperCase());
/*    */     } else {
/* 52 */       this.player.sendMessage(Lang.getPrefix() + ChatColor.RED + "You do " + ChatColor.UNDERLINE + "NOT" + ChatColor.RED + " have enough credits");
/*    */     }
/*    */     
/*    */ 
/*    */ 
/* 57 */     PlayerManager.getMessageMap().remove(this.player.getUniqueId().toString());
/*    */   }
/*    */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/structure/MessageResult.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */