/*    */ package me.xCyanide.claimlevels.structure;
/*    */ 
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ import java.util.UUID;
/*    */ import me.xCyanide.claimlevels.ClaimLevels;
/*    */ import me.xCyanide.claimlevels.gui.RedeemGUI;
/*    */ import me.xCyanide.claimlevels.io.FileManager;
/*    */ import me.xCyanide.claimlevels.utils.Callback;
/*    */ import me.xCyanide.claimlevels.utils.UUIDLibrary;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.Server;
/*    */ import org.bukkit.configuration.file.FileConfiguration;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.scheduler.BukkitScheduler;
/*    */ 
/*    */ public class PlayerManager
/*    */ {
/* 19 */   private static Map<String, CPlayer> playerMap = new HashMap();
/*    */   
/* 21 */   private static Map<String, MessageResult> messageMap = new HashMap();
/*    */   
/* 23 */   private static Map<String, RedeemGUI> openGUIs = new HashMap();
/*    */   
/*    */   public static Map<String, CPlayer> getPlayerMap() {
/* 26 */     return playerMap;
/*    */   }
/*    */   
/*    */   public static Map<String, MessageResult> getMessageMap() {
/* 30 */     return messageMap;
/*    */   }
/*    */   
/*    */   public static Map<String, RedeemGUI> getOpenGUIs() {
/* 34 */     return openGUIs;
/*    */   }
/*    */   
/*    */   public static void loadPlayer(Player player) {
/* 38 */     CPlayer cPlayer = new CPlayer(player);
/* 39 */     playerMap.put(player.getUniqueId().toString(), cPlayer);
/*    */   }
/*    */   
/*    */   public static void unloadPlayer(Player player) {
/* 43 */     playerMap.remove(player.getUniqueId().toString());
/* 44 */     messageMap.remove(player.getUniqueId().toString());
/* 45 */     openGUIs.remove(player.getUniqueId().toString());
/*    */   }
/*    */   
/*    */   public static void getCreditsAsync(final String name, final Callback<Integer> cb) {
/* 49 */     Player player = Bukkit.getPlayer(name);
/* 50 */     ClaimLevels.getInstance().getServer().getScheduler().runTaskAsynchronously(ClaimLevels.getInstance(), new Runnable()
/*    */     {
/*    */       public void run()
/*    */       {
/* 54 */         String uuid = "";
/*    */         
/* 56 */         int credits = -1;
/*    */         
/* 58 */         if (this.val$player != null) {
/* 59 */           uuid = this.val$player.getUniqueId().toString();
/*    */         } else {
/* 61 */           uuid = UUIDLibrary.getUUIDFromName(name);
/*    */         }
/*    */         
/* 64 */         if (uuid != null) {
/* 65 */           if (PlayerManager.playerMap.containsKey(uuid)) {
/* 66 */             credits = ((CPlayer)PlayerManager.playerMap.get(uuid)).getCredits();
/*    */           }
/* 68 */           else if (FileManager.getDataConfig().contains(uuid)) {
/* 69 */             credits = FileManager.getDataConfig().getInt(uuid + ".Credits");
/*    */           }
/*    */         }
/*    */         
/*    */ 
/* 74 */         cb.done(Integer.valueOf(credits));
/*    */       }
/*    */     });
/*    */   }
/*    */   
/*    */   public static void getUUIDAsync(String name, final Callback<String> cb) {
/* 80 */     ClaimLevels.getInstance().getServer().getScheduler().runTaskAsynchronously(ClaimLevels.getInstance(), new Runnable()
/*    */     {
/*    */       public void run() {
/* 83 */         String uuid = UUIDLibrary.getUUIDFromName(this.val$name);
/* 84 */         cb.done(uuid);
/*    */       }
/*    */     });
/*    */   }
/*    */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/structure/PlayerManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */