/*    */ package me.xCyanide.claimlevels.structure;
/*    */ 
/*    */ import java.util.UUID;
/*    */ import me.xCyanide.claimlevels.io.FileManager;
/*    */ import org.bukkit.configuration.file.FileConfiguration;
/*    */ import org.bukkit.entity.Player;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CPlayer
/*    */ {
/*    */   private Player player;
/*    */   private int credits;
/*    */   
/*    */   public CPlayer(Player player)
/*    */   {
/* 17 */     this.player = player;
/* 18 */     FileConfiguration config = FileManager.getDataConfig();
/* 19 */     if (config.contains(player.getUniqueId().toString() + ".credits")) {
/* 20 */       this.credits = config.getInt(player.getUniqueId().toString() + ".credits");
/*    */     } else {
/* 22 */       this.credits = 0;
/*    */     }
/*    */   }
/*    */   
/*    */   public Player getPlayer() {
/* 27 */     return this.player;
/*    */   }
/*    */   
/*    */   public int getCredits() {
/* 31 */     return this.credits;
/*    */   }
/*    */   
/*    */   public void addCredits(int amount) {
/* 35 */     this.credits += amount;
/* 36 */     FileConfiguration config = FileManager.getDataConfig();
/* 37 */     config.set(this.player.getUniqueId().toString() + ".credits", Integer.valueOf(this.credits));
/* 38 */     FileManager.saveDataConfig();
/*    */   }
/*    */   
/*    */   public void removeCredits(int amount) {
/* 42 */     this.credits -= amount;
/* 43 */     FileConfiguration config = FileManager.getDataConfig();
/* 44 */     config.set(this.player.getUniqueId().toString() + ".credits", Integer.valueOf(this.credits));
/* 45 */     FileManager.saveDataConfig();
/*    */   }
/*    */ }


/* Location:              /Users/student/Downloads/ClaimLevels.jar!/me/xCyanide/claimlevels/structure/CPlayer.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */